package cn.weixin.song.controller.api;

import javax.servlet.http.HttpServletRequest;

import com.github.sd4324530.fastweixin.servlet.WeixinSupport;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.tx.Tx;


public class WxController extends Controller {
	
	@Before(Tx.class)
    public void index() {
            HttpServletRequest request = getRequest();
            WeixinSupport support = new MyWeixinSupport(this.getPara()); 
            //绑定微信服务器
            if ("GET".equalsIgnoreCase(request.getMethod().toUpperCase())) {
                support.bindServer(request, getResponse());
                renderNull();
            } else {
                //处理消息
                renderText(support.processRequest(request), "text/xml");
            }
        }
}
